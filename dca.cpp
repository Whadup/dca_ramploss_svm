/*
 * Train an SVM for MNIST digits 5 vs. 6 with ramp loss using DCA.
 * Formally we solve min_w 0.5*λ * ||w||² + 1/n ∑ min(1,max(0,1-y w'x))
 * to train the linear SVM model regularized with 0.5λ||w||².
 * Compile and Run:
 * g++ -O3 -march=native -o dca dca.cpp && ./dca
 */
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <vector>
#include <utility>
#include <math.h>
#include <stdlib.h>

int n;
int m;
std::vector<double*> data;
std::vector<int> label;
std::vector<double*> testdata;
std::vector<int> testlabel;
double* w; //x
double* y; //y
double lambda=0.01;
int positive = 1;
int negative = 7;
bool realRandom = false;
double R = 0;
void loadMNistData(std::vector<double*>& dataset, std::vector<int>& labels, std::vector<double*>& testset, std::vector<int>& testLabels)
{
	std::ifstream is("mnist_train.csv");
	unsigned int width=28*28+1;
	  
	int d;
	int i = 0;
	double* read = new double[width];
	while (!is.eof())
	{
		int l=0;
		is >> l;

		// std::cout << l << " ";
		is.ignore(1);
		bool flip=0;
		if(rand()%10==1)
			flip=1;
		double rtmp=0;
		for(int i=0;i<width-1;i++)
		{
			is >> d;
			if(flip)
				read[i] = d/16.0; //make every ~10th vector really large.
			else
				read[i] = d/256.0;
			rtmp+=read[i]*read[i];
			is.ignore(1);
		}
		read[width-1]=1;
		// std::cout << read.transpose() << std::endl;
		if(l==positive || l==negative)
		{
			labels.push_back(l==negative ? -1 : 1);
			dataset.push_back(read);
			// std::cout << ".";
		}
		else
		{
			delete[] read;
		}
		rtmp=sqrt(rtmp);
		if(rtmp>R)
			R=rtmp;
		read = new double[width];
	}
	is.close();

	std::ifstream tis("mnist_test.csv");	  
	i = 0;
	read = new double[width];
	while (!tis.eof())
	{
		int l=0;
		tis >> l;

		tis.ignore(1);
		bool flip=0;
		if(rand()%10==1)
			flip=1;
		for(int i=0;i<width-1;i++)
		{
			tis >> d;
			if(flip)
				read[i] = d/16.0; //make every ~10th vector really large.
			else
				read[i] = d/256.0;
			tis.ignore(1);
		}
		read[width-1]=1;
		// std::cout << read.transpose() << std::endl;
		if(l==positive || l==negative)
		{
			testLabels.push_back(l==negative ? -1 : 1);
			testset.push_back(read);
			// std::cout << ".";
		}
		else
		{
			delete[] read;
		}
		read = new double[width];
	}

	tis.close();
}

double oneStochasticSubGradientStep(int x,double alpha)
{
	double pred = 0;
	for(int i=0;i<m;i++)
	{
		pred+=w[i]*data[x][i];
		//don't regularize bias.
		if(i!=m-1)
			w[i]-=alpha*lambda*w[i];
		w[i]+=alpha*y[i];
	}
	if(label[x]*pred<1)
	{
		for(int i=0;i<m;i++)
		{
			w[i]+=alpha*label[x]*data[x][i];
		}
		double loss = 1-label[x]*pred;
		if(loss>1)
			loss=1;
		return loss;
	}
	return 0;
}
double testloss(int x)
{
	double pred = 0;
	for(int i=0;i<m;i++)
	{
		pred+=w[i]*testdata[x][i];
	}
	// std::cout << pred << std::endl;
	if(testlabel[x]*pred<1)
	{
		double loss = 1-testlabel[x]*pred;
		if(loss>1)
			loss=1;
		return loss;
	}
	return 0;
}

double trainloss(int x)
{
	double pred = 0;
	for(int i=0;i<m;i++)
	{
		pred+=w[i]*data[x][i];
	}
	// std::cout << pred << std::endl;
	if(label[x]*pred<1)
	{
		double loss = 1-label[x]*pred;
		if(loss>1)
			loss=1;
		return loss;
	}
	return 0;
}

void computeY()
{
	for(int j=0;j<m;j++)
		y[j] = 0;
	for(int i=0;i<n;i++)
	{
		double pred = 0;
		for(int j=0;j<m;j++)
		{
			pred+=data[i][j]*w[j];
		}
		if(label[i]*pred<0)
		{
			for(int j=0;j<m;j++)
				y[j]-=data[i][j]*label[i];
		}
	}
	for(int j=0;j<m;j++)
	{
		y[j]/=n;
	//	w[j]=0;
	}
}

int main()
{
	m=28*28+1;
	loadMNistData(data,label,testdata,testlabel);
	std::cout << "loaded mnist and fucked up some examples"<<std::endl;
	n=data.size();
	w = new double[m];
	y = new double[m];
	for(int i=0;i<m;i++)
		w[i] = y[i] = 0;
	std::cout << "λ: "<<lambda<<std::endl;
	std::cout << "Train\tTest\tNorm" << std::fixed << std::setprecision(5)<<std::endl;
	for(int k=0;k<20;k++)
	{
		if(realRandom)
		{
			for(int j=0;j<1500*n;j++) //1000 epochs. It is really important that this converges. So it needs ~1/lambda iterations.
			{
				int i=rand()%n;
				oneStochasticSubGradientStep(i,1.0/((j+1)*lambda));
			}
		}
		else
		{
			for(int j=0;j<1500;j++) //1000 epochs. It is really important that this converges. So it needs ~1/lambda iterations.
				for(int i=0;i<n;i++)
					oneStochasticSubGradientStep(i,1.0/((j*n+i+1)*lambda));
		}
		
		//Evaluate current model
		double loss=0;
		for(int i=0;i<n;i++)
			loss+=trainloss(i);
		std::cout << loss/n << "\t";
		double train = loss;
		loss = 0;
		for(int i=0;i<testdata.size();i++)
			loss+=testloss(i);		
		std::cout << loss/testdata.size() << "\t";//std::endl;
		loss=0;
		for(int i=0;i<m-1;i++)
			loss+=w[i]*w[i];
		std::cout << sqrt(loss) << "\t";//std::endl;
		std::cout << train+2*R*sqrt(loss)/sqrt(n) << std::endl;

		//DCA: Pick y^k
		computeY();
	}
	return 0;
}