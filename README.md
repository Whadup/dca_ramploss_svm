
# README #

  Train an SVM for MNIST digits 5 vs. 6 with ramp loss using DCA.
  
  Formally we solve min_w 0.5*λ * ||w||² + 1/n ∑ min(1,max(0,1-y w'x))
  
  to train the linear SVM model regularized with 0.5λ||w||².
  
  To simulate some outlying data points, we rescale about 1 in 10 data points by a factor of 16.
  
## Compile and Run: ##
  `g++ -O3 -march=native -o dca dca.cpp && ./dca`

## Literature ##
* Collobert et al., "Trading Convexity for Scalability", ICML, 2006, http://machinelearning.wustl.edu/mlpapers/paper_files/icml2006_CollobertSWB06.pdf
* H. A. Le Thi, T. Pham Dinh, H. M. Le, and X. T. Vo, “DC approximation Approaches for Sparse Optimization,” European Journal for Operations Research, 2015, http://www.fr-hermite.univ-lorraine.fr/sites/www.fr-hermite.univ-lorraine.fr/files/Journee_scientifique_23062014/lethi.pdf